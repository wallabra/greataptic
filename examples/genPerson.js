const greataptic = require('..');

const noiseSize = 20;
const netType = 'linear';

const names = ['Jake', 'Ivan', 'Ivy', 'Olivia', 'Livia', 'Lind', 'Jason', 'James', 'Jack', 'Jane', 'Jonathan', 'Jonas', 'Jon', 'Mark', 'Amanda', 'Rob', 'Robert', 'Paul', 'Piolo', 'Paula', 'Paolo', 'Icarus', 'Ichigo', 'Dick', 'Zurg', 'Zora', 'Anna', 'Tim', 'Timmy', 'Trevor', 'Rick', 'Richard', 'Heinrich', 'Dickinson', 'Trevalian', 'Nabucodonossor', 'Delta', 'Davida', 'Santos', 'Pearlie', 'Ghislaine', 'Grant', 'Lavern', 'Estell', 'Adah', 'Sharan', 'Josephine', 'Hope', 'Jonathon', 'Rutha', 'Stephine', 'Amee', 'Germaine', 'Leif', 'Arlie', 'Linnea', 'Windy', 'Madeline', 'Fletcher', 'Hortencia', 'Jolene', 'Ali', 'Jeannine', 'Kyoko', 'Kathaleen', 'Zola', 'Rosalee', 'Lelia', 'Lorilee', 'Mi', 'Tora', 'Chantelle', 'Claude', 'Bobbie', 'Dorthey', 'Miyoko', 'Caroyln', 'Bethann', 'Loise', 'Jaimie', 'Mirna', 'Krysten', 'Herma', 'Carolyn', 'Nannie', 'Donetta', 'Joleen', 'Joana', 'Enriqueta', 'Penny', 'Trish', 'Margert', 'Elza', 'Gonzalo', 'Pauline', 'Minh', 'Takisha', 'Eugenio', 'Yuko', 'Rosaline', 'Roni', 'Cherise', 'Evette', 'Lyle', 'Apryl', 'Kathey', 'Danilo', 'Shaunte', 'Donna', 'Vincent', 'Somer', 'Lilia', 'Oda', 'Aurelio', 'Pok', 'Kazuko', 'Trevor', 'Ardis', 'Maris', 'Zulma', 'Nanci', 'Berniece', 'Marya', 'Hunter', 'Danette', 'Eliza', 'Tennille', 'Donn', 'Marina', 'Lieselotte', 'Dinorah', 'Deeann', 'Frankie', 'Cecila', 'Rickey', 'Jeanmarie', 'Lakendra', 'Emerald', 'Conchita', 'Claudio', 'Eleonore', 'Marylin', 'Wynona', 'Meda', 'Graig', 'Thomasina', 'Shayna', 'Leigha', 'Gaynelle', 'Jovan', 'Maira', 'Celinda', 'Argentina', 'Shawnta', 'Lea', 'Jeannine', 'Valorie', 'Lavette', 'Lily', 'Edmund', 'Jude', 'Zelda', 'Lannie', 'Geri', 'Paulette', 'Mirtha', 'Cherise', 'Catrina', 'Marva', 'Linette', 'Enda', 'Santo', 'Erick', 'Tona', 'Colin', 'Felicita', 'Alecia', 'Fermina', 'Birdie', 'Jamar', 'Cyndy', 'Taren', 'Judson', 'Eliseo', 'Isaias', 'Dell'];
const minAge = 18;
const maxAge = 60;
const minChildren = 0;
const maxChildren = 7;

function randint(mn, mx) {
    return Math.floor(Math.random() * (1 + mx - mn) + mn);
}

function randrange(mn, mx) {
    return Math.random() * (mx - mn) + mn;
}

let personVec = greataptic.createVectorifier([
    {
        name: 'name',
        type: 'simplestring',
        
        size: Math.max.apply(Math, names.map((n) => n.length))
    },

    {
        name: 'age',
        type: 'number',

        min: minAge,
        max: maxAge
    },

    {
        name: 'children',
        type: 'number',

        min: minChildren,
        max: maxChildren,

        rounded: true
    }
]);

let gan = new greataptic.GAN({
    size: {
        output: personVec.getSize(),
        noise: noiseSize
    },

    outputType: netType
});

let realSet = names.map((n) => {
    let dp0 = {
        name: n,
        age: randrange(minAge, maxAge),
        children: randint(minChildren, maxChildren),
    };
    let p = personVec.encode(dp0);

    console.log(`Example person: ${dp0.name}, ${dp0.age}yrs, ${dp0.children} children`);

    return p;
});

let maxRes = (isNaN(+process.argv[2]) ? 15 : +process.argv[2]);

console.log(' * [INFO] Training.');
gan.evolve(realSet, {
    maxGens: 200,
    population: 80,
    maxComparisonSize: 90,
    fitnessQuota: 0.95,
    maxMutation: 0.2,
    debug: true,

    discriminatorTrainOptions: {
        population: 50,
        fitnessQuota: 0.95
    },

    postStep: (net) => {
        for (let i = 1; i <= maxRes / 3; i++) {
            let gen = gan.generate(net);
            let person = personVec.decode(gen);
            console.log(`Name: ${person.name}`);
            console.log(`Age: ${Math.round(person.age)}y`);
            console.log(`Children: ${Math.round(person.children)}`);
            console.log(`Data: ${gen.join('|')}`);
    
            if (i < maxRes)
                console.log('-------------------\n');
        }
    }
}).then(() => {
    console.log('\n\nResults:\n');

    for (let i = 1; i <= maxRes; i++) {
        let person = personVec.decode(gan.generate());
        console.log(`Name: ${person.name}`);
        console.log(`Age: ${Math.round(person.age)}y`);
        console.log(`Children: ${Math.round(person.children)}`);

        if (i < maxRes)
            console.log('\n-------------------\n');
    }
});

module.exports = {
    gan: gan
};