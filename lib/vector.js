function _logit(x) {
    return Math.log(x / (1 - x));
}

function $vec(v) {
    if (v.data && v.dims)
        v = v.data;

    let w = {
        data: Array.from(v),
        dims: v.length,

        co: function(coord) {
            return v[coord] || null;
        },

        size: function() {
            return Math.sqrt(w.data.reduce((a, b) => a + Math.pow(b, 2), 0));
        },

        sum: function() {
            return w.data.reduce((a, b) => a + b, 0);
        },

        add: function (b) {
            if (!b.dims) b = $vec(b);

            return $vec(w.data.map((x, i) => x + b.data[i]));
        },

        pow: function (pow) {
            return $vec(w.data.map((x) => Math.pow(x, pow)));
        },

        sub: function (b) {
            if (!b.dims) b = $vec(b);

            return $vec(w.data.map((x, i) => x - b.data[i]));
        },

        multiplyVec: function (b) {
            if (!b.dims) b = $vec(b);

            return $vec(w.data.map((x, i) => x * b.data[i]));
        },

        divideVec: function (b) {
            if (!b.dims) b = $vec(b);

            return $vec(w.data.map((x, i) => x / b.data[i]));
        },

        multiplyFac: function (b) {
            return $vec(w.data.map((a) => a * b));
        },

        divideFac: function (b) {
            return $vec(w.data.map((a) => a / b));
        },

        combiner: function (combiner) {
            return function (b) {
                if (!b.dims) b = $vec(b);

                return $vec(w.data.map((x, i) => combiner(x, b.data[i])));
            };
        },

        map: function (func) {
            return $vec(w.data.map(func));
        },

        dot: function (b) {
            if (w.data.length === 0 || b.dims === 0)
                return 0;

            return w.data.slice(0, Math.min(v.length, b.dims)).map((a, i) => b.co(i) * a).reduce((a, b) => a + b, 0);
            
        }
    };

    return w;
}

$vec.is = function (vec) {
    return !!(vec && vec.data && vec.dims && vec.size && vec.combiner && vec.map);
};

$vec.fill = function (opts) {
    if (opts.map != null)
        return $vec(new Array(opts.length).fill(0).map((_, i, a) => opts.map(i, a)));

    else
        return $vec(new Array(opts.length).fill(opts.value != null ? opts.value : 0));
};

$vec.random = function (dims) {
    return $vec(new Array(dims).fill(0).map(() => _logit(0.25 + 0.5 * Math.random())));
};

$vec.randomOne = function () {
    return _logit(0.25 + 0.5 * Math.random());
};

$vec.randomSize = function (dims, size) {
    return $vec(new Array(dims).fill(0).map(() => (Math.random() * 2 - 1) * size));
};

$vec.zero = function(dims) {
    return $vec(new Array(dims).fill(0));
};


module.exports = $vec;