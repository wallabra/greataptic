require=(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
if (typeof Object.create === 'function') {
  // implementation from standard node.js 'util' module
  module.exports = function inherits(ctor, superCtor) {
    ctor.super_ = superCtor
    ctor.prototype = Object.create(superCtor.prototype, {
      constructor: {
        value: ctor,
        enumerable: false,
        writable: true,
        configurable: true
      }
    });
  };
} else {
  // old school shim for old browsers
  module.exports = function inherits(ctor, superCtor) {
    ctor.super_ = superCtor
    var TempCtor = function () {}
    TempCtor.prototype = superCtor.prototype
    ctor.prototype = new TempCtor()
    ctor.prototype.constructor = ctor
  }
}

},{}],2:[function(require,module,exports){
// shim for using process in browser
var process = module.exports = {};

// cached from whatever global is present so that test runners that stub it
// don't break things.  But we need to wrap it in a try catch in case it is
// wrapped in strict mode code which doesn't define any globals.  It's inside a
// function because try/catches deoptimize in certain engines.

var cachedSetTimeout;
var cachedClearTimeout;

function defaultSetTimout() {
    throw new Error('setTimeout has not been defined');
}
function defaultClearTimeout () {
    throw new Error('clearTimeout has not been defined');
}
(function () {
    try {
        if (typeof setTimeout === 'function') {
            cachedSetTimeout = setTimeout;
        } else {
            cachedSetTimeout = defaultSetTimout;
        }
    } catch (e) {
        cachedSetTimeout = defaultSetTimout;
    }
    try {
        if (typeof clearTimeout === 'function') {
            cachedClearTimeout = clearTimeout;
        } else {
            cachedClearTimeout = defaultClearTimeout;
        }
    } catch (e) {
        cachedClearTimeout = defaultClearTimeout;
    }
} ())
function runTimeout(fun) {
    if (cachedSetTimeout === setTimeout) {
        //normal enviroments in sane situations
        return setTimeout(fun, 0);
    }
    // if setTimeout wasn't available but was latter defined
    if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
        cachedSetTimeout = setTimeout;
        return setTimeout(fun, 0);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedSetTimeout(fun, 0);
    } catch(e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
            return cachedSetTimeout.call(null, fun, 0);
        } catch(e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
            return cachedSetTimeout.call(this, fun, 0);
        }
    }


}
function runClearTimeout(marker) {
    if (cachedClearTimeout === clearTimeout) {
        //normal enviroments in sane situations
        return clearTimeout(marker);
    }
    // if clearTimeout wasn't available but was latter defined
    if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
        cachedClearTimeout = clearTimeout;
        return clearTimeout(marker);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedClearTimeout(marker);
    } catch (e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
            return cachedClearTimeout.call(null, marker);
        } catch (e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
            // Some versions of I.E. have different rules for clearTimeout vs setTimeout
            return cachedClearTimeout.call(this, marker);
        }
    }



}
var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
    if (!draining || !currentQueue) {
        return;
    }
    draining = false;
    if (currentQueue.length) {
        queue = currentQueue.concat(queue);
    } else {
        queueIndex = -1;
    }
    if (queue.length) {
        drainQueue();
    }
}

function drainQueue() {
    if (draining) {
        return;
    }
    var timeout = runTimeout(cleanUpNextTick);
    draining = true;

    var len = queue.length;
    while(len) {
        currentQueue = queue;
        queue = [];
        while (++queueIndex < len) {
            if (currentQueue) {
                currentQueue[queueIndex].run();
            }
        }
        queueIndex = -1;
        len = queue.length;
    }
    currentQueue = null;
    draining = false;
    runClearTimeout(timeout);
}

process.nextTick = function (fun) {
    var args = new Array(arguments.length - 1);
    if (arguments.length > 1) {
        for (var i = 1; i < arguments.length; i++) {
            args[i - 1] = arguments[i];
        }
    }
    queue.push(new Item(fun, args));
    if (queue.length === 1 && !draining) {
        runTimeout(drainQueue);
    }
};

// v8 likes predictible objects
function Item(fun, array) {
    this.fun = fun;
    this.array = array;
}
Item.prototype.run = function () {
    this.fun.apply(null, this.array);
};
process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues
process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;
process.prependListener = noop;
process.prependOnceListener = noop;

process.listeners = function (name) { return [] }

process.binding = function (name) {
    throw new Error('process.binding is not supported');
};

process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};
process.umask = function() { return 0; };

},{}],3:[function(require,module,exports){
'use strict';

/**
 * Randomize the order of the elements in a given array.
 * @param {Array} arr - The given array.
 * @param {Object} [options] - Optional configuration options.
 * @param {Boolean} [options.copy] - Sets if should return a shuffled copy of the given array. By default it's a falsy value.
 * @param {Function} [options.rng] - Specifies a custom random number generator.
 * @returns {Array}
 */
function shuffle(arr, options) {

  if (!Array.isArray(arr)) {
    throw new Error('shuffle expect an array as parameter.');
  }

  options = options || {};

  var collection = arr,
      len = arr.length,
      rng = options.rng || Math.random,
      random,
      temp;

  if (options.copy === true) {
    collection = arr.slice();
  }

  while (len) {
    random = Math.floor(rng() * len);
    len -= 1;
    temp = collection[len];
    collection[len] = collection[random];
    collection[random] = temp;
  }

  return collection;
};

/**
 * Pick one or more random elements from the given array.
 * @param {Array} arr - The given array.
 * @param {Object} [options] - Optional configuration options.
 * @param {Number} [options.picks] - Specifies how many random elements you want to pick. By default it picks 1.
 * @param {Function} [options.rng] - Specifies a custom random number generator.
 * @returns {Object}
 */
shuffle.pick = function(arr, options) {

  if (!Array.isArray(arr)) {
    throw new Error('shuffle.pick() expect an array as parameter.');
  }

  options = options || {};

  var rng = options.rng || Math.random,
      picks = options.picks || 1;

  if (typeof picks === 'number' && picks !== 1) {
    var len = arr.length,
        collection = arr.slice(),
        random = [],
        index;

    while (picks && len) {
      index = Math.floor(rng() * len);
      random.push(collection[index]);
      collection.splice(index, 1);
      len -= 1;
      picks -= 1;
    }

    return random;
  }

  return arr[Math.floor(rng() * arr.length)];
};

/**
 * Expose
 */
module.exports = shuffle;

},{}],4:[function(require,module,exports){
module.exports = function isBuffer(arg) {
  return arg && typeof arg === 'object'
    && typeof arg.copy === 'function'
    && typeof arg.fill === 'function'
    && typeof arg.readUInt8 === 'function';
}
},{}],5:[function(require,module,exports){
(function (process,global){
// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.

var formatRegExp = /%[sdj%]/g;
exports.format = function(f) {
  if (!isString(f)) {
    var objects = [];
    for (var i = 0; i < arguments.length; i++) {
      objects.push(inspect(arguments[i]));
    }
    return objects.join(' ');
  }

  var i = 1;
  var args = arguments;
  var len = args.length;
  var str = String(f).replace(formatRegExp, function(x) {
    if (x === '%%') return '%';
    if (i >= len) return x;
    switch (x) {
      case '%s': return String(args[i++]);
      case '%d': return Number(args[i++]);
      case '%j':
        try {
          return JSON.stringify(args[i++]);
        } catch (_) {
          return '[Circular]';
        }
      default:
        return x;
    }
  });
  for (var x = args[i]; i < len; x = args[++i]) {
    if (isNull(x) || !isObject(x)) {
      str += ' ' + x;
    } else {
      str += ' ' + inspect(x);
    }
  }
  return str;
};


// Mark that a method should not be used.
// Returns a modified function which warns once by default.
// If --no-deprecation is set, then it is a no-op.
exports.deprecate = function(fn, msg) {
  // Allow for deprecating things in the process of starting up.
  if (isUndefined(global.process)) {
    return function() {
      return exports.deprecate(fn, msg).apply(this, arguments);
    };
  }

  if (process.noDeprecation === true) {
    return fn;
  }

  var warned = false;
  function deprecated() {
    if (!warned) {
      if (process.throwDeprecation) {
        throw new Error(msg);
      } else if (process.traceDeprecation) {
        console.trace(msg);
      } else {
        console.error(msg);
      }
      warned = true;
    }
    return fn.apply(this, arguments);
  }

  return deprecated;
};


var debugs = {};
var debugEnviron;
exports.debuglog = function(set) {
  if (isUndefined(debugEnviron))
    debugEnviron = process.env.NODE_DEBUG || '';
  set = set.toUpperCase();
  if (!debugs[set]) {
    if (new RegExp('\\b' + set + '\\b', 'i').test(debugEnviron)) {
      var pid = process.pid;
      debugs[set] = function() {
        var msg = exports.format.apply(exports, arguments);
        console.error('%s %d: %s', set, pid, msg);
      };
    } else {
      debugs[set] = function() {};
    }
  }
  return debugs[set];
};


/**
 * Echos the value of a value. Trys to print the value out
 * in the best way possible given the different types.
 *
 * @param {Object} obj The object to print out.
 * @param {Object} opts Optional options object that alters the output.
 */
/* legacy: obj, showHidden, depth, colors*/
function inspect(obj, opts) {
  // default options
  var ctx = {
    seen: [],
    stylize: stylizeNoColor
  };
  // legacy...
  if (arguments.length >= 3) ctx.depth = arguments[2];
  if (arguments.length >= 4) ctx.colors = arguments[3];
  if (isBoolean(opts)) {
    // legacy...
    ctx.showHidden = opts;
  } else if (opts) {
    // got an "options" object
    exports._extend(ctx, opts);
  }
  // set default options
  if (isUndefined(ctx.showHidden)) ctx.showHidden = false;
  if (isUndefined(ctx.depth)) ctx.depth = 2;
  if (isUndefined(ctx.colors)) ctx.colors = false;
  if (isUndefined(ctx.customInspect)) ctx.customInspect = true;
  if (ctx.colors) ctx.stylize = stylizeWithColor;
  return formatValue(ctx, obj, ctx.depth);
}
exports.inspect = inspect;


// http://en.wikipedia.org/wiki/ANSI_escape_code#graphics
inspect.colors = {
  'bold' : [1, 22],
  'italic' : [3, 23],
  'underline' : [4, 24],
  'inverse' : [7, 27],
  'white' : [37, 39],
  'grey' : [90, 39],
  'black' : [30, 39],
  'blue' : [34, 39],
  'cyan' : [36, 39],
  'green' : [32, 39],
  'magenta' : [35, 39],
  'red' : [31, 39],
  'yellow' : [33, 39]
};

// Don't use 'blue' not visible on cmd.exe
inspect.styles = {
  'special': 'cyan',
  'number': 'yellow',
  'boolean': 'yellow',
  'undefined': 'grey',
  'null': 'bold',
  'string': 'green',
  'date': 'magenta',
  // "name": intentionally not styling
  'regexp': 'red'
};


function stylizeWithColor(str, styleType) {
  var style = inspect.styles[styleType];

  if (style) {
    return '\u001b[' + inspect.colors[style][0] + 'm' + str +
           '\u001b[' + inspect.colors[style][1] + 'm';
  } else {
    return str;
  }
}


function stylizeNoColor(str, styleType) {
  return str;
}


function arrayToHash(array) {
  var hash = {};

  array.forEach(function(val, idx) {
    hash[val] = true;
  });

  return hash;
}


function formatValue(ctx, value, recurseTimes) {
  // Provide a hook for user-specified inspect functions.
  // Check that value is an object with an inspect function on it
  if (ctx.customInspect &&
      value &&
      isFunction(value.inspect) &&
      // Filter out the util module, it's inspect function is special
      value.inspect !== exports.inspect &&
      // Also filter out any prototype objects using the circular check.
      !(value.constructor && value.constructor.prototype === value)) {
    var ret = value.inspect(recurseTimes, ctx);
    if (!isString(ret)) {
      ret = formatValue(ctx, ret, recurseTimes);
    }
    return ret;
  }

  // Primitive types cannot have properties
  var primitive = formatPrimitive(ctx, value);
  if (primitive) {
    return primitive;
  }

  // Look up the keys of the object.
  var keys = Object.keys(value);
  var visibleKeys = arrayToHash(keys);

  if (ctx.showHidden) {
    keys = Object.getOwnPropertyNames(value);
  }

  // IE doesn't make error fields non-enumerable
  // http://msdn.microsoft.com/en-us/library/ie/dww52sbt(v=vs.94).aspx
  if (isError(value)
      && (keys.indexOf('message') >= 0 || keys.indexOf('description') >= 0)) {
    return formatError(value);
  }

  // Some type of object without properties can be shortcutted.
  if (keys.length === 0) {
    if (isFunction(value)) {
      var name = value.name ? ': ' + value.name : '';
      return ctx.stylize('[Function' + name + ']', 'special');
    }
    if (isRegExp(value)) {
      return ctx.stylize(RegExp.prototype.toString.call(value), 'regexp');
    }
    if (isDate(value)) {
      return ctx.stylize(Date.prototype.toString.call(value), 'date');
    }
    if (isError(value)) {
      return formatError(value);
    }
  }

  var base = '', array = false, braces = ['{', '}'];

  // Make Array say that they are Array
  if (isArray(value)) {
    array = true;
    braces = ['[', ']'];
  }

  // Make functions say that they are functions
  if (isFunction(value)) {
    var n = value.name ? ': ' + value.name : '';
    base = ' [Function' + n + ']';
  }

  // Make RegExps say that they are RegExps
  if (isRegExp(value)) {
    base = ' ' + RegExp.prototype.toString.call(value);
  }

  // Make dates with properties first say the date
  if (isDate(value)) {
    base = ' ' + Date.prototype.toUTCString.call(value);
  }

  // Make error with message first say the error
  if (isError(value)) {
    base = ' ' + formatError(value);
  }

  if (keys.length === 0 && (!array || value.length == 0)) {
    return braces[0] + base + braces[1];
  }

  if (recurseTimes < 0) {
    if (isRegExp(value)) {
      return ctx.stylize(RegExp.prototype.toString.call(value), 'regexp');
    } else {
      return ctx.stylize('[Object]', 'special');
    }
  }

  ctx.seen.push(value);

  var output;
  if (array) {
    output = formatArray(ctx, value, recurseTimes, visibleKeys, keys);
  } else {
    output = keys.map(function(key) {
      return formatProperty(ctx, value, recurseTimes, visibleKeys, key, array);
    });
  }

  ctx.seen.pop();

  return reduceToSingleString(output, base, braces);
}


function formatPrimitive(ctx, value) {
  if (isUndefined(value))
    return ctx.stylize('undefined', 'undefined');
  if (isString(value)) {
    var simple = '\'' + JSON.stringify(value).replace(/^"|"$/g, '')
                                             .replace(/'/g, "\\'")
                                             .replace(/\\"/g, '"') + '\'';
    return ctx.stylize(simple, 'string');
  }
  if (isNumber(value))
    return ctx.stylize('' + value, 'number');
  if (isBoolean(value))
    return ctx.stylize('' + value, 'boolean');
  // For some reason typeof null is "object", so special case here.
  if (isNull(value))
    return ctx.stylize('null', 'null');
}


function formatError(value) {
  return '[' + Error.prototype.toString.call(value) + ']';
}


function formatArray(ctx, value, recurseTimes, visibleKeys, keys) {
  var output = [];
  for (var i = 0, l = value.length; i < l; ++i) {
    if (hasOwnProperty(value, String(i))) {
      output.push(formatProperty(ctx, value, recurseTimes, visibleKeys,
          String(i), true));
    } else {
      output.push('');
    }
  }
  keys.forEach(function(key) {
    if (!key.match(/^\d+$/)) {
      output.push(formatProperty(ctx, value, recurseTimes, visibleKeys,
          key, true));
    }
  });
  return output;
}


function formatProperty(ctx, value, recurseTimes, visibleKeys, key, array) {
  var name, str, desc;
  desc = Object.getOwnPropertyDescriptor(value, key) || { value: value[key] };
  if (desc.get) {
    if (desc.set) {
      str = ctx.stylize('[Getter/Setter]', 'special');
    } else {
      str = ctx.stylize('[Getter]', 'special');
    }
  } else {
    if (desc.set) {
      str = ctx.stylize('[Setter]', 'special');
    }
  }
  if (!hasOwnProperty(visibleKeys, key)) {
    name = '[' + key + ']';
  }
  if (!str) {
    if (ctx.seen.indexOf(desc.value) < 0) {
      if (isNull(recurseTimes)) {
        str = formatValue(ctx, desc.value, null);
      } else {
        str = formatValue(ctx, desc.value, recurseTimes - 1);
      }
      if (str.indexOf('\n') > -1) {
        if (array) {
          str = str.split('\n').map(function(line) {
            return '  ' + line;
          }).join('\n').substr(2);
        } else {
          str = '\n' + str.split('\n').map(function(line) {
            return '   ' + line;
          }).join('\n');
        }
      }
    } else {
      str = ctx.stylize('[Circular]', 'special');
    }
  }
  if (isUndefined(name)) {
    if (array && key.match(/^\d+$/)) {
      return str;
    }
    name = JSON.stringify('' + key);
    if (name.match(/^"([a-zA-Z_][a-zA-Z_0-9]*)"$/)) {
      name = name.substr(1, name.length - 2);
      name = ctx.stylize(name, 'name');
    } else {
      name = name.replace(/'/g, "\\'")
                 .replace(/\\"/g, '"')
                 .replace(/(^"|"$)/g, "'");
      name = ctx.stylize(name, 'string');
    }
  }

  return name + ': ' + str;
}


function reduceToSingleString(output, base, braces) {
  var numLinesEst = 0;
  var length = output.reduce(function(prev, cur) {
    numLinesEst++;
    if (cur.indexOf('\n') >= 0) numLinesEst++;
    return prev + cur.replace(/\u001b\[\d\d?m/g, '').length + 1;
  }, 0);

  if (length > 60) {
    return braces[0] +
           (base === '' ? '' : base + '\n ') +
           ' ' +
           output.join(',\n  ') +
           ' ' +
           braces[1];
  }

  return braces[0] + base + ' ' + output.join(', ') + ' ' + braces[1];
}


// NOTE: These type checking functions intentionally don't use `instanceof`
// because it is fragile and can be easily faked with `Object.create()`.
function isArray(ar) {
  return Array.isArray(ar);
}
exports.isArray = isArray;

function isBoolean(arg) {
  return typeof arg === 'boolean';
}
exports.isBoolean = isBoolean;

function isNull(arg) {
  return arg === null;
}
exports.isNull = isNull;

function isNullOrUndefined(arg) {
  return arg == null;
}
exports.isNullOrUndefined = isNullOrUndefined;

function isNumber(arg) {
  return typeof arg === 'number';
}
exports.isNumber = isNumber;

function isString(arg) {
  return typeof arg === 'string';
}
exports.isString = isString;

function isSymbol(arg) {
  return typeof arg === 'symbol';
}
exports.isSymbol = isSymbol;

function isUndefined(arg) {
  return arg === void 0;
}
exports.isUndefined = isUndefined;

function isRegExp(re) {
  return isObject(re) && objectToString(re) === '[object RegExp]';
}
exports.isRegExp = isRegExp;

function isObject(arg) {
  return typeof arg === 'object' && arg !== null;
}
exports.isObject = isObject;

function isDate(d) {
  return isObject(d) && objectToString(d) === '[object Date]';
}
exports.isDate = isDate;

function isError(e) {
  return isObject(e) &&
      (objectToString(e) === '[object Error]' || e instanceof Error);
}
exports.isError = isError;

function isFunction(arg) {
  return typeof arg === 'function';
}
exports.isFunction = isFunction;

function isPrimitive(arg) {
  return arg === null ||
         typeof arg === 'boolean' ||
         typeof arg === 'number' ||
         typeof arg === 'string' ||
         typeof arg === 'symbol' ||  // ES6 symbol
         typeof arg === 'undefined';
}
exports.isPrimitive = isPrimitive;

exports.isBuffer = require('./support/isBuffer');

function objectToString(o) {
  return Object.prototype.toString.call(o);
}


function pad(n) {
  return n < 10 ? '0' + n.toString(10) : n.toString(10);
}


var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep',
              'Oct', 'Nov', 'Dec'];

// 26 Feb 16:19:34
function timestamp() {
  var d = new Date();
  var time = [pad(d.getHours()),
              pad(d.getMinutes()),
              pad(d.getSeconds())].join(':');
  return [d.getDate(), months[d.getMonth()], time].join(' ');
}


// log is just a thin wrapper to console.log that prepends a timestamp
exports.log = function() {
  console.log('%s - %s', timestamp(), exports.format.apply(exports, arguments));
};


/**
 * Inherit the prototype methods from one constructor into another.
 *
 * The Function.prototype.inherits from lang.js rewritten as a standalone
 * function (not on Function.prototype). NOTE: If this file is to be loaded
 * during bootstrapping this function needs to be rewritten using some native
 * functions as prototype setup using normal JavaScript does not work as
 * expected during bootstrapping (see mirror.js in r114903).
 *
 * @param {function} ctor Constructor function which needs to inherit the
 *     prototype.
 * @param {function} superCtor Constructor function to inherit prototype from.
 */
exports.inherits = require('inherits');

exports._extend = function(origin, add) {
  // Don't do anything if add isn't an object
  if (!add || !isObject(add)) return origin;

  var keys = Object.keys(add);
  var i = keys.length;
  while (i--) {
    origin[keys[i]] = add[keys[i]];
  }
  return origin;
};

function hasOwnProperty(obj, prop) {
  return Object.prototype.hasOwnProperty.call(obj, prop);
}

}).call(this,require('_process'),typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./support/isBuffer":4,"_process":2,"inherits":1}],"ga-vector":[function(require,module,exports){
function _logit(x) {
    return Math.log(x / (1 - x));
}

function $vec(v) {
    if (v.data && v.dims)
        v = v.data;

    let w = {
        data: Array.from(v),
        dims: v.length,

        co: function(coord) {
            return v[coord] || null;
        },

        size: function() {
            return Math.sqrt(w.data.reduce((a, b) => a + Math.pow(b, 2), 0));
        },

        sum: function() {
            return w.data.reduce((a, b) => a + b, 0);
        },

        add: function (b) {
            if (!b.dims) b = $vec(b);

            return $vec(w.data.map((x, i) => x + b.data[i]));
        },

        pow: function (pow) {
            return $vec(w.data.map((x) => Math.pow(x, pow)));
        },

        sub: function (b) {
            if (!b.dims) b = $vec(b);

            return $vec(w.data.map((x, i) => x - b.data[i]));
        },

        multiplyVec: function (b) {
            if (!b.dims) b = $vec(b);

            return $vec(w.data.map((x, i) => x * b.data[i]));
        },

        divideVec: function (b) {
            if (!b.dims) b = $vec(b);

            return $vec(w.data.map((x, i) => x / b.data[i]));
        },

        multiplyFac: function (b) {
            return $vec(w.data.map((a) => a * b));
        },

        divideFac: function (b) {
            return $vec(w.data.map((a) => a / b));
        },

        combiner: function (combiner) {
            return function (b) {
                if (!b.dims) b = $vec(b);

                return $vec(w.data.map((x, i) => combiner(x, b.data[i])));
            };
        },

        map: function (func) {
            return $vec(w.data.map(func));
        },

        dot: function (b) {
            if (w.data.length === 0 || b.dims === 0)
                return 0;

            return w.data.slice(0, Math.min(v.length, b.dims)).map((a, i) => b.co(i) * a).reduce((a, b) => a + b, 0);
            
        }
    };

    return w;
}

$vec.is = function (vec) {
    return !!(vec && vec.data && vec.dims && vec.size && vec.combiner && vec.map);
};

$vec.fill = function (opts) {
    if (opts.map != null)
        return $vec(new Array(opts.length).fill(0).map((_, i, a) => opts.map(i, a)));

    else
        return $vec(new Array(opts.length).fill(opts.value != null ? opts.value : 0));
};

$vec.random = function (dims) {
    return $vec(new Array(dims).fill(0).map(() => _logit(0.25 + 0.5 * Math.random())));
};

$vec.randomOne = function () {
    return _logit(0.25 + 0.5 * Math.random());
};

$vec.randomSize = function (dims, size) {
    return $vec(new Array(dims).fill(0).map(() => (Math.random() * 2 - 1) * size));
};

$vec.zero = function(dims) {
    return $vec(new Array(dims).fill(0));
};


module.exports = $vec;
},{}],"greataptic":[function(require,module,exports){
(function (process){
var util = require('util');
var $vec = require('./lib/vector.js');
var shuffle = require('shuffle-array');

let words = ['above-mentioned', 'above-listed', 'before-mentioned', 'aforementioned', 'abundance', 'accelerate', 'accentuate', 'accommodation', 'accompany', 'accomplish', 'accorded', 'accordingly', 'accrue', 'accurate', 'acquiesce', 'acquire', 'additional', 'address', 'addressees', 'adjustment', 'admissible', 'advantageous', 'advise', 'aggregate', 'aircraft', 'alleviate', 'allocate', 'alternatively', 'ameliorate', 'and/or', 'anticipate', 'applicant', 'application', 'apparent', 'apprehend', 'appreciable', 'appropriate', 'approximate', 'ascertain', 'attain', 'attempt', 'authorize', 'beg', 'belated', 'beneficial', 'bestow', 'beverage', 'capability', 'caveat', 'cease', 'chauffeur', 'clearly', 'obviously', 'combined', 'commence', 'complete', 'component', 'comprise', 'conceal', 'concerning', 'consequently', 'consolidate', 'constitutes', 'contains', 'convene', 'corridor', 'currently', 'deem', 'delete', 'demonstrate', 'depart', 'designate', 'desire', 'determine', 'disclose', 'different', 'discontinue', 'disseminate', 'duly', 'authorized', 'signed', 'each...apiece', 'economical', 'elect', 'eliminate', 'elucidate', 'emphasize', 'employ', 'encounter', 'endeavor', 'end', 'result', 'product', 'enquiry', 'ensure', 'entitlement', 'enumerate', 'equipments', 'equitable', 'equivalent', 'establish', 'evaluate', 'evidenced', 'evident', 'evince', 'excluding', 'exclusively', 'exhibit', 'expedite', 'expeditious', 'expend', 'expertise', 'expiration', 'facilitate', 'fauna', 'feasible', 'females', 'finalize', 'flora', 'following', 'forfeit', 'formulate', 'forward', 'frequently', 'function', 'furnish', 'grant', 'herein', 'heretofore', 'herewith', 'thereof', 'wherefore', 'wherein', 'however', 'identical', 'identify', 'immediately', 'impacted', 'implement', 'inasmuch', 'inception', 'indicate', 'indication', 'initial', 'initiate', 'interface', 'irregardless', 'liaison', '-ly', 'doubtless', 'fast', 'ill', 'much', 'seldom', 'thus', 'magnitude', 'maintain', 'majority', 'maximum', 'merge', 'methodology', 'minimize', 'minimum', 'modify', 'monitor', 'moreover', 'multiple', 'necessitate', 'nevertheless', 'notify', 'not...unless', 'not...except', 'not...until', 'notwithstanding', 'numerous', 'objective', 'obligate', 'observe', 'obtain', 'operate', 'optimum', 'option', 'orientate', '...out', 'calculate', 'cancel,', 'distribute', 'segregate', 'separate', 'overall', 'parameters', 'participate', 'particulars', 'perchance', 'perform', 'permit', 'perspire', 'peruse', 'place', 'portion', 'possess', 'potentiality', 'practicable', 'preclude', 'preowned', 'previously', 'prioritize', 'proceed', 'procure', 'proficiency', 'promulgate', 'provide', 'purchase', 'reflect', 'regarding', 'relocate', 'remain', 'remainder', 'remuneration', 'render', 'represents', 'request', 'require', 'requirement', 'reside', 'residence', 'respectively', 'retain', 'retire', 'rigorous', 'selection', 'separate', 'shall', 'solicit', 'state-of-the-art', 'strategize', 'subject', 'submit', 'subsequent', 'subsequently', 'substantial', 'sufficient', 'terminate', 'therefore', 'therein', 'timely', 'transpire', 'transmit', 'type', 'validate', 'variation', 'very', 'viable', 'warrant', 'whereas', 'whosoever', 'whomsoever', 'witnessed'];

function _logit(x) {
    return Math.log(x / (1 - x));
}

function gaussRand() {
    // adapted from https://stackoverflow.com/a/49434653/5129091
    var u = Math.random(), v = Math.random();

    if (u === 0) u = 0.5;
    if (v === 0) v = 0.5;

    let res = Math.sqrt( -2.0 * Math.log(u) ) * Math.cos( 2.0 * Math.PI * v ) / 10 + .5;
    if (res > 1 || res < 0) return gaussRand();

    return res;
}

let greataptic = {};
greataptic.$vec = $vec;

function mutateNum(x, amount) {
    return x + 2 * amount * (Math.random() - 0.5);
}

function mutateList(l, amount) {
    return l.map((x) => mutateNum(x, amount));
}

function stepInterp(a, b, alpha) {
    return (1 - alpha) * a + alpha * b;
}

function stepInterpList(la, lb, alpha) {
    return la.map((a, i) => stepInterp(a, lb[i], alpha));
}

let types = greataptic.layerTypes = {
    sequence: {
        process: (vec, layer) => {
            return $vec(layer.parts.reduce((a, l) => {
                return types[l.type].process(a, l);
            }, vec));
        },

        build: (parts, next) => ({
            parts: parts,
            size: parts.map((l) => l.size),
            type: 'sequence',
            next: next
        }),

        mutate: (layer, amount = null) => {
            layer.parts.forEach((l) => {
                if (l.type.mutate)
                    types[l.type].mutate(l, amount);
            });
        },

        breed: (layer1, layer2) => {
            let p = layer1.parts.map((l, i) => types[l.type].breed(l, layer2.parts[i]));

            return types.sequence.build(p, layer1.next);
        },

        applyStep: (layer1, layer2, fitness) => {
            let p = layer1.parts.map((l, i) => types[l.type].applyStep(l, layer2.parts[i], fitness));

            return types.sequence.build(p, layer1.next);
        }
    },

    combo: {
        process: (vec, layer) => {
            let res = layer.parts.reduce((a, ln) => a.concat(types[ln.type].process(vec, ln).data), []);
            return $vec(res);
        },

        build: (parts, next) => ({
            parts: parts,
            size: parts.reduce((a, b) => a + b.size, 0),
            type: 'combo',
            next: next
        }),

        mutate: (layer, amount = null) => {
            layer.parts.forEach((l) => {
                if (l.mutate)
                    types[l.type].mutate(l, amount);
            });
        },

        breed: (layer1, layer2) => {
            let p = layer1.parts.map((l, i) => types[l.type].breed(l, layer2.parts[i]));

            return types.combo.build(p, layer1.next);
        },

        applyStep: (layer1, layer2, fitness) => {
            let p = layer1.parts.map((l, i) => types[l.type].applyStep(l, layer2.parts[i], fitness));

            return types.combo.build(p, layer1.next);
        }
    },

    sigmoid: {
        process: (vec) => {
            return vec.map((x) => 1 / (1 + Math.exp(-x)));
        },

        build: (next) => ({
            data: null,
            type: 'sigmoid',
            next: next
        }),

        mutate: function(){},
        breed: (layer) => layer,
        applyStep: (layer) => layer,

        activate: (n) => {
            return 1 / (1 + Math.exp(-n));
        },
    },

    tanh: {
        process: (vec) => {
            return vec.map((x) => Math.tanh(x));
        },

        build: (next) => ({
            data: null,
            type: 'tanh',
            next: next
        }),

        mutate: function(){},
        breed: (layer) => layer,
        applyStep: (layer) => layer,

        activate: (n) => {
            return Math.tanh(n);
        },
    },

    logit: {
        process: (vec) => {
            return vec.map((x) => Math.log(x / (1 - x)));
        },

        build: (next) => ({
            data: null,
            type: 'logit',
            next: next
        }),

        mutate: function(){},

        breed: (layer) => {
            return layer;
        },

        applyStep: (layer) => {
            return layer;
        },

        activate: (n) => {
            return Math.log(n / (1 - n));
        },
    },

    spiking: {
        process: (vec, layer) => {
            vec = $vec(vec);
            let res = new Array(layer.data.length).fill(0);

            layer.data.forEach((node, ni) => {
                if ((node.power += Math.max($vec(node.weights).multiplyVec(vec).sum(), 0)) > node.limit) {
                    node.power = 0;
                    res[ni] = node.output;
                }
            });

            return $vec(res);
        },

        build: (nodes, next) => ({
            data: nodes,
            type: 'spiking',
            next: next
        }),

        mutate: (layer, amount = null) => {
            function _mut(x) {
                if (amount != null)
                    return x + 2 * amount * (Math.random() - 0.5);

                else
                    return x + _logit(0.25 + 0.5 * Math.random());
            }

            layer.data = layer.data.map((n) => ({
                power: n.power,
                output: n.output,
                limit: _mut(n.limit),
                weights: n.weights.map(_mut)
            }));
        },

        breed: (layer1, layer2) => {
            return {
                data: layer1.data.map((node, ni) => ({
                    power: choice([node.power, layer2.data[ni].power]),
                    output: choice([node.output, layer2.data[ni].output]),
                    limit: choice([node.limit, layer2.data[ni].limit]),
                    weights: node.weights.map((x, xi) => Math.random() > 0.5 ? x : layer2.data[ni].weights[xi])
                })),

                type: 'spiking',
                next: layer1.next,
                name: layer1.name,
            };
        },

        applyStep: (layer1, layer2, fitness) => {
            return {
                data: layer1.data.map((node, ni) => ({
                    power: stepInterp(node.power, layer2.data[ni].power, fitness),
                    output: stepInterp(node.output, layer2.data[ni].output, fitness),
                    limit: stepInterp(node.limit, layer2.data[ni].limit, fitness),
                    weights: node.weights.map((x, xi) => stepInterp(x, layer2.data[ni].weights[xi], fitness))
                })),

                type: 'spiking',
                next: layer1.next,
                name: layer1.name,
            };
        }
    },

    square: {
        process: (vec, layer) => {
            vec = $vec(vec);
            return $vec(layer.data.map((n) => {
                return $vec(n.weights.linear).multiplyVec(vec).add($vec(n.weights.square).multiplyVec(vec.pow(2))).sum() + n.offset;
            }));
        },

        build: (nodes, next) => ({
            data: nodes,
            type: 'square',
            next: next
        }),

        mutate: (layer, amount = null) => {
            function _mut(x) {
                if (amount != null)
                    return x + 2 * amount * (Math.random() - 0.5);

                else
                    return x + _logit(0.25 + 0.5 * Math.random());
            }

            layer.data = layer.data.map((l) => ({
                weights: {
                    linear: l.weights.linear.map(_mut),
                    square: l.weights.square.map(_mut),
                },
                offset: _mut(l.offset)
            }));
        },

        breed: (layer1, layer2) => {
            return {
                data: layer1.data.map((n, ni) => ({
                    offset: choice([n.offset, layer2.data[ni].offset]),
                    weights: {
                        linear: n.weights.linear.map((x, xi) => (Math.random() > 0.5) ? x : layer2.data[ni].weights.linear[xi]),
                        square: n.weights.square.map((x, xi) => (Math.random() > 0.5) ? x : layer2.data[ni].weights.square[xi])
                    }
                })),

                type: 'square',
                next: layer1.next,
                name: layer1.name,
            };
        },

        applyStep: (layer1, layer2, fitness) => {
            return {
                data: layer1.data.map((n, ni) => ({
                    offset: stepInterp(n.offset, layer2.data[ni].offset, fitness),
                    weights: {
                        linear: n.weights.linear.map((x, xi) => stepInterp(x, layer2.data[ni].weights.linear[xi], fitness)),
                        square: n.weights.square.map((x, xi) => stepInterp(x, layer2.data[ni].weights.square[xi], fitness))
                    }
                })),

                type: 'square',
                next: layer1.next,
                name: layer1.name,
            };
        }
    },

    linear: {
        process: (vec, layer) => {
            vec = $vec(vec);
            return $vec(layer.data.map((n) => {
                let res = $vec(n.weights).multiplyVec(vec).sum() + n.offset;
                return res;
            }));
        },

        build: (nodes, next) => {
            return {
                data: nodes,
                type: 'linear',
                next: next
            };
        },

        mutate: (layer, amount = null) => {
            function _mut(x) {
                if (amount != null)
                    return x + 2 * amount * (Math.random() - 0.5);

                else
                    return x + _logit(0.25 + 0.5 * Math.random());
            }

            layer.data = layer.data.map((l) => ({
                weights: l.weights.map(_mut),
                offset: _mut(l.offset)
            }));
        },

        breed: (layer1, layer2) => {
            return {
                data: layer1.data.map((n, ni) => ({
                    offset: choice([n.offset, layer2.data[ni].offset]),
                    weights: n.weights.map((x, xi) => (Math.random() > 0.5) ? x : layer2.data[ni].weights[xi])
                })),

                type: 'linear',
                next: layer1.next,
                name: layer1.name,
            };
        },

        applyStep: (layer1, layer2, fitness) => {
            return {
                data: layer1.data.map((n, ni) => ({
                    offset: stepInterp(n.offset, layer2.data[ni].offset, fitness),
                    weights: n.weights.map((x, xi) => stepInterp(x , layer2.data[ni].weights[xi], fitness))
                })),

                type: 'linear',
                next: layer1.next,
                name: layer1.name,
            };
        },
    },

    lstm: {},
};

function addLstmType(name, props) {
    let res = {
        randomGates: (lastSize, size) => {
            return {
                input: {
                    weights: new Array(size).fill(0).map(() => $vec.random(lastSize).data),
                    hiddenWeights: new Array(size).fill(0).map(() => $vec.random(lastSize).data),
                    offset: $vec.random(size).data,
                },
                
                output: {
                    weights: new Array(size).fill(0).map(() => $vec.random(lastSize).data),
                    hiddenWeights: new Array(size).fill(0).map(() => $vec.random(lastSize).data),
                    offset: $vec.random(size).data,
                },
                
                forget: {
                    weights: new Array(size).fill(0).map(() => $vec.random(lastSize).data),
                    hiddenWeights: new Array(size).fill(0).map(() => $vec.random(lastSize).data),
                    offset: $vec.random(size).data,
                },

                cell: {
                    weights: new Array(size).fill(0).map(() => $vec.random(lastSize).data),
                    hiddenWeights: new Array(size).fill(0).map(() => $vec.random(lastSize).data),
                    offset: $vec.random(size).data,
                },
            };
        },

        randomStates: (size) => {
            return {
                cell: new Array(size).fill(0),
                hidden: new Array(size).fill(0),
            };
        },

        build: (gates, states, next, activation = 'sigmoid', cellActivation = 'tanh') => {
            return {
                gates: gates,
                states: states,

                activation: activation,
                cellActivation: cellActivation,

                type: 'lstm',
                next: next,
            };
        },

        process: (vec, layer) => {
            vec = $vec(vec);
            
            let { forget, input, output } = layer.gates;
            let gCell = layer.gates.cell;
            let act = layer.activation;
            let cAct = layer.cellActivation;

            let sHidden = $vec(layer.states.hidden);
            let sCell = $vec(layer.states.cell);

            let gat = {
                forget: {
                    weights: forget.weights.map((w) => $vec(w)),
                    hiddenWeights: forget.hiddenWeights.map((w) => $vec(w)),
                },

                input: {
                    weights: input.weights.map((w) => $vec(w)),
                    hiddenWeights: input.hiddenWeights.map((w) => $vec(w))
                },

                output: {
                    weights: output.weights.map((w) => $vec(w)),
                    hiddenWeights: output.hiddenWeights.map((w) => $vec(w))
                },

                cell: {
                    weights: gCell.weights.map((w) => $vec(w)),
                    hiddenWeights: gCell.hiddenWeights.map((w) => $vec(w))
                },
            };

            let forgVec = $vec(gat.forget.weights.map((n, i) => (
                greataptic.activate(act, n.multiplyVec(vec).sum() + gat.forget.hiddenWeights[i].multiplyVec(sHidden).sum() + forget.offset[i])
            )));

            let inpVec = $vec(gat.input.weights.map((n, i) => (
                greataptic.activate(act, n.multiplyVec(vec).sum() + gat.input.hiddenWeights[i].multiplyVec(sHidden).sum() + input.offset[i])
            )));

            let outVec = $vec(gat.output.weights.map((n, i) => (
                greataptic.activate(act, n.multiplyVec(vec).sum() + gat.output.hiddenWeights[i].multiplyVec(sHidden).sum() + output.offset[i])
            )));

            layer.states.cell = forgVec.multiplyVec(sCell).add(inpVec.multiplyVec(
                gat.cell.weights.map((n, i) => (
                    greataptic.activate(cAct, n.multiplyVec(vec).sum() + gat.cell.hiddenWeights[i].multiplyVec(sHidden).sum() + gCell.offset[i])
                ))
            )).data;

            layer.states.hidden = outVec.multiplyVec(layer.states.cell.map((x) => greataptic.activate(cAct, x))).data;

            return $vec(layer.states.hidden);
        },

        breedGate: (g1, g2, choices) => {
            return {
                weights: g1.weights.map((w, i) => choices[i] ? w : g2.weights[i]),
                hiddenWeights: g1.hiddenWeights.map((w, i) => choices[i] ? w : g2.hiddenWeights[i]),
                offset: g1.offset.map((o, i) => choices[i] ? o : g2.offset[i])
            };
        },

        breedGates: (t, g1, g2, choices) => {
            if (!choices) {
                for (let i = 0; i < g1.hidden.weights.length; i++)
                    choices.push(+(Math.random() > 0.5));
            }

            return {
                hidden: types[t].breedGate(g1.hidden, g2.hidden, choices),
                input: types[t].breedGate(g1.input, g2.input, choices),
                output: types[t].breedGate(g1.output, g2.output, choices),
                cell: types[t].breedGate(g1.cell, g2.cell, choices),
            };
        },

        breedState: (s1, s2, choices) => {
            return s1.map((x, i) => choices[i] ? x : s2[i]);
        },

        breedStates: (t, s1, s2, choices) => {
            return {
                cell: types[t].breedState(s1.cell, s2.cell, choices),
                hidden: types[t].breedState(s1.hidden, s2.hidden, choices),
            };
        },

        breed: (layer1, layer2) => {
            let choices = [];
            let bred = types[layer1.type].build(
                types[layer1.type].breedGates(layer1.type, layer1.gates, layer2.gates, choices),
                types[layer1.type].breedStates(layer1.type, layer1.states, layer2.states, choices),
                layer1.next,
                layer1.activation,
                layer1.cellActivation
            );

            bred.name = layer1.name;
            
            return bred;
        },

        stepGate: (g1, g2, fit) => {
            return {
                weights: g1.weights.map((w, i) => stepInterpList(w, g2.weights[i], fit)),
                hiddenWeights: g1.hiddenWeights.map((w, i) => stepInterpList(w, g2.hiddenWeights[i], fit)),
                offset: stepInterpList(g1.offset, g2.offset, fit)
            };
        },

        stepGates: (t, g1, g2, fit) => {
            return {
                forget: types[t].stepGate(g1.forget, g2.forget, fit),
                input: types[t].stepGate(g1.input, g2.input, fit),
                output: types[t].stepGate(g1.output, g2.output, fit),
                cell: types[t].stepGate(g1.cell, g2.cell, fit),
            };
        },

        stepState: (s1, s2, fit) => {
            return s1.map((x, i) => stepInterp(x, s2[i], fit));
        },

        stepStates: (t, s1, s2, fit) => {
            return {
                cell: types[t].stepState(s1.cell, s2.cell, fit),
                hidden: types[t].stepState(s1.hidden, s2.hidden, fit),
            };
        },

        applyStep: (layer1, layer2, fit) => {
            let stepped = types[layer1.type].build(
                types[layer1.type].stepGates(layer1.type, layer1.gates, layer2.gates, fit),
                types[layer1.type].stepStates(layer1.type, layer1.states, layer2.states, fit),
                layer1.next,
                layer1.activation,
                layer1.cellActivation
            );

            stepped.name = layer1.name;
            
            return stepped;
        },

        mutateGate: (g1, amount) => {
            return {
                weights: g1.weights.map((w) => mutateList(w, amount)),
                hiddenWeights: g1.hiddenWeights.map((w) => mutateList(w, amount)),
                offset: mutateList(g1.offset, amount)
            };
        },

        mutateGates: (t, g1, amount) => {
            g1.forget = types[t].mutateGate(g1.forget, amount);
            g1.input = types[t].mutateGate(g1.input, amount);
            g1.output = types[t].mutateGate(g1.output, amount);
            g1.cell = types[t].mutateGate(g1.cell, amount);
        },

        mutateState: (s1, amount) => {
            return mutateList(s1, amount);
        },

        mutateStates: (t, s1, amount) => {
            s1.cell = types[t].mutateState(s1.cell, amount);
            s1.hidden = types[t].mutateState(s1.hidden, amount);
        },

        mutate: (l, amount) => {
            types[l.type].mutateGates(l.type, l.gates, amount);
            types[l.type].mutateStates(l.type, l.states, amount);
        }
    };

    Object.assign(res, props);
    types[name] = res;
}

addLstmType('lstm', {});
addLstmType('lstm_peephole', {
    build: (gates, states, next, activation = 'sigmoid', cellActivation = 'tanh') => {
        return {
            gates: gates,
            states: states,

            activation: activation,
            cellActivation: cellActivation,

            type: 'lstm',
            next: next,
        };
    },
    
    process: (vec, layer) => {
        vec = $vec(vec);
        
        let { forget, input, output } = layer.gates;
        let gCell = layer.gates.cell;
        let act = layer.activation;
        let cAct = layer.cellActivation;

        let sCell = $vec(layer.states.cell);

        let gat = {
            forget: {
                weights: forget.weights.map((w) => $vec(w)),
                hiddenWeights: forget.hiddenWeights.map((w) => $vec(w)),
            },

            input: {
                weights: input.weights.map((w) => $vec(w)),
                hiddenWeights: input.hiddenWeights.map((w) => $vec(w))
            },

            output: {
                weights: output.weights.map((w) => $vec(w)),
                hiddenWeights: output.hiddenWeights.map((w) => $vec(w))
            },

            cell: {
                weights: gCell.weights.map((w) => $vec(w)),
                hiddenWeights: gCell.hiddenWeights.map((w) => $vec(w))
            },
        };

        let forgVec = $vec(gat.forget.weights.map((n, i) => (
            greataptic.activate(act, n.multiplyVec(vec).sum() + gat.forget.hiddenWeights[i].multiplyVec(sCell).sum() + forget.offset[i])
        )));

        let inpVec = $vec(gat.input.weights.map((n, i) => (
            greataptic.activate(act, n.multiplyVec(vec).sum() + gat.input.hiddenWeights[i].multiplyVec(sCell).sum() + input.offset[i])
        )));

        let outVec = $vec(gat.output.weights.map((n, i) => (
            greataptic.activate(act, n.multiplyVec(vec).sum() + gat.output.hiddenWeights[i].multiplyVec(sCell).sum() + output.offset[i])
        )));

        layer.states.cell = forgVec.multiplyVec(sCell).add(inpVec.multiplyVec(
            gat.cell.weights.map((n, i) => (
                greataptic.activate(cAct, n.multiplyVec(vec).sum() + gCell.offset[i])
            ))
        )).data;

        layer.states.hidden = outVec.multiplyVec(layer.states.cell.map((x) => greataptic.activate(cAct, x))).data;

        return $vec(layer.states.hidden);
    },
});



greataptic.activate = function(kind, n) {
    if (!kind) return n;
    return types[kind].activate(n);
};

greataptic.isNetwork = function(n) {
    return !!(n && n.layers && (n.first != null));
};

function choice(l) {
    return l[Math.floor(l.length * Math.random())];
}

greataptic.breed = function breed(nets) {
    let res = nets[0].clone();
    let newLayers = {};

    Object.keys(res.data.layers).forEach((i) => {
        let l = res.data.layers[i];

        let n2 = choice(nets.slice(1));
        let l2 = n2.data.layers[i];

        if (n2 !== nets[0] && l2.type === l.type && types[l.type].breed)
            l = types[l.type].breed(l, l2);

        newLayers[i] = l;
    });

    res.data.layers = newLayers;
    return res;
};

greataptic.$net = function $net(net) {
    if (!greataptic.isNetwork(net))
        throw new Error(`The net parameter passed to greataptic.$net (${util.inspect(net)}) is invalid!`);

    let w = {
        data: net,
        json: JSON.stringify.bind(JSON, net),
        id: new Array(5).fill(0).map(() => choice(words)).join('-') + '-' + Math.ceil(Math.random() * 10000),

        compute: function compute(vec) {
            if (!$vec.is(vec))
                vec = $vec(vec);

            let li;
            let layer = net.layers[li = net.first];
            let res = vec;
            let i = 0;

            // eslint-disable-next-line no-constant-condition
            while (true) {
                if (layer == null)
                    throw new Error(`Layer '${li}' in network pool (#${i + 1} in sequence) does not exist!`);

                let t = types[layer.type];

                if (t == null)
                    throw new Error(`No such layer type ${util.inspect(layer.type)}!`);

                //let old = res;
                layer.net = this;
                res = t.process(res, layer);
                delete layer.net;

                if (layer.next == null)
                    return res;

                else {
                    layer = net.layers[li = layer.next];
                    i++;
                }
            }
        },

        computeAsync: function compute(vec) {
            if (!$vec.is(vec))
                vec = $vec(vec);

            let li;
            let layer = net.layers[li = net.first];
            let res = vec;
            let i = 0;

            return new Promise((resolve) => {
            // eslint-disable-next-line no-constant-condition
                function pass() {
                    if (layer == null)
                        throw new Error(`Layer '${li}' in network pool (#${i + 1} in sequence) does not exist!`);

                    let t = types[layer.type];

                    if (t == null)
                        throw new Error(`No such layer type ${util.inspect(layer.type)}!`);

                    //let old = res;
                    layer.net = this;
                    res = t.process(res, layer);
                    delete layer.net;

                    if (layer.next == null)
                        resolve(res);

                    else {
                        layer = net.layers[li = layer.next];
                        i++;

                        setTimeout(pass, 0);
                    }
                }

                pass();
            });
        },

        clone: function clone() {
            return $net(JSON.parse(w.json()));
        },

        mutate: function mutate(amount = null) {
            let c = w.clone();

            Array.from(Object.entries(c.data.layers)).forEach((e) => {
                let l = e[1];

                if (l.type && types[l.type].mutate) {
                    types[l.type].mutate(l, amount);
                }
            });

            return c;
        },

        applyStep: function applyStep(otherNet, fitness) {
            if (fitness === 0) return;

            Array.from(Object.entries(w.data.layers)).forEach((e) => {
                let id = e[0];
                let l = e[1];

                if (l.type && types[l.type].applyStep && otherNet.data.layers[id] && otherNet.data.layers[id].type === l.type)
                    w.data.layers[id] = types[l.type].applyStep(l, otherNet.data.layers[id], fitness);
            });
        },

        error: function error(inputSet, expectedSet) {
            let outputSet = inputSet.map((i) => w.compute(i));
            let res = outputSet.map((os, si) => os.data.map((o, i) => Math.pow((o - expectedSet[si][i]), 2)).reduce((a, b) => a + b, 0)).reduce((a, b) => a + b, 0);

            return res / inputSet.length;
        },

        staticFitness: function staticFitness(inputSet, expectedSet) {
            let err = w.error(inputSet, expectedSet);
            return 1 / (1 + err);
        },

        train: function train(options = {}) {
            let quota = options.fitnessQuota || 0.9;
            let cb = (options.inputSet == null || options.expectedSet == null) ? options.fitnessCallback : function (n) {
                return n.staticFitness(options.inputSet, options.expectedSet);
            };
            let pool = new Array(options.population || 50).fill(0).map(() => w.mutate());

            // function f(x) { return 1 / (1 + x); }
            for (let generation = 1; generation <= (options.maxGens != null ? options.maxGens : 500); generation++) {
                let fits = {};

                pool.forEach((n, i, arr) => {
                    if (options.debug)
                        (options.log || process.stdout.write.bind(process.stdout))(`\r(${i + 1}/${arr.length})`);

                    fits[n.id] = cb(n, generation);
                });
                pool = pool.sort((a, b) => fits[b.id] - fits[a.id]);

                let maxFit = Math.max.apply(Math, Object.keys(fits).map((k) => fits[k]));
                let worstFit = Math.min.apply(Math, Object.keys(fits).map((k) => fits[k]));

                if (options.debug)
                    (options.log || process.stdout.write.bind(process.stdout))(`\r[Generation #${generation}] Best fitness: ${maxFit} | Worst fitness: ${worstFit}\n`);

                if (maxFit >= quota)
                    return pool[0];

                let desiredLength = pool.length;

                pool = pool.slice(0, Math.ceil(pool.length * (options.survivalRate || 0.5)));
                let best = pool.slice(0, Math.ceil(pool.length * (options.elitism != null ? options.elitism : 0.15))).concat(pool.slice(Math.ceil(pool.length * (options.elitism != null ? options.elitism : 0.15))).map((n) => n.mutate(options.mutation || null)));
                pool = Array.from(best);

                while (pool.length < desiredLength)
                    pool.push(greataptic.breed(best));

                if (options.generationCallback != null)
                    options.generationCallback({
                        gen: generation,
                        best: pool[0],
                        bestFit: maxFit,
                        worstFit: worstFit,
                        pool: pool
                    });
            }

            let best = pool[0];
            let bf = cb(best);

            pool.slice(1).forEach((n) => {
                let f = cb(n);

                if (f > bf) {
                    bf = f;
                    best = n;
                }
            });

            return best;
        },

        _evolveStep: function _evolveStep(ctx) {
            function populateSteps() {
                let res = ctx.steps = new Array(ctx.population).fill(0).map(() => ctx.makeStep(w));
                return res;
            }

            function tryStep(step) {
                return Promise.resolve(ctx.getFitness(step)).then((fit) => {
                    step.fitness = fit;
                });
            }

            function concludeSteps() {
                let res = w.clone();
                let newFit = 0;
                let denom = 1;
                
                ctx.steps.forEach((s) => {
                    res.applyStep(s, s.fitness * ctx.stepFactor);

                    newFit = (newFit * (denom - 1) + s.fitness) / denom++;
                });
                
                res.fitness = newFit;
                res.trainCtx = ctx;

                if (ctx.options.postStep)
                    ctx.options.postStep(res);
                
                return res;
            }

            populateSteps();

            let prom = Promise.resolve();

            if (ctx.options.stepAll) {
                let batchPop = ctx.options.batchPop;
                let i = 0;
                let batchIndex = 1;

                // eslint-disable-next-line no-constant-condition
                while (true) {
                    if (i >= ctx.steps.length)
                        break;

                    batchPop = Math.min(ctx.steps.length - i, batchPop);
                    
                    let ls = ctx.steps.slice(i, i + batchPop);
                    let gen = ctx.options.generation || null;
                    let bi = batchIndex;
                    
                    prom = prom.then(() => Promise.resolve(ctx.options.stepAll(ls, gen, bi)));
                    
                    i += batchPop;
                    batchIndex++;
                }
            }
            
            else
                ctx.steps.forEach((step) => {
                    prom = prom.then(() => tryStep(step));
                });

            return new Promise((resolve) => {
                prom.then(() => {
                    resolve(concludeSteps());
                });
            });
        },

        evolveStep: function evolveStep(options = {}) {
            function getFitness(net) {
                return new Promise(() => {
                    if (options.step)
                        return Promise.resolve(options.step(net, options.generation || null));

                    else
                        return 0;
                        
                }).then((res) => {
                    if (options.inputSet && options.expectedSet && !options.callbackOnly)
                        res += net.staticFitness(options.inputSet, options.expectedSet);

                    return res;
                });
            }

            function makeStep() {
                if (options.random)
                    return w.mutate(100);

                else
                    return w.mutate(gaussRand() * ctx.maxMutation * Math.pow(options.mutationDecay, (options.generation || 0) - 1));
            }

            let ctx = {
                options: options,
                
                population: options.population || 50,
                maxGens: options.maxGens || 500,
                maxMutation: options.maxMutation || 0.5,
                mutationDecay: options.mutationDecay || 0.85,
                stepFactor: options.stepFactor || 0.75,
                
                getFitness: getFitness,
                makeStep: makeStep,

                steps: null
            };

            return w._evolveStep(ctx);
        },

        evolve: function evolve(options = {}) {
            let fitQuota = options.quota != null ? options.quota : Infinity;
            let cur = w;
            let gen = 1;

            options.random = options.random || false;
            
            return new Promise((resolve) => {
                function iter() {
                    options.generation = gen;
                    cur.evolveStep(options).then((evolved) => {
                        options.random = false;

                        if (options.debug) // print debug status
                            console.log(`[DEBUG] Generation ${gen}/${evolved.trainCtx.maxGens}: fitness is now ${evolved.fitness}.`);

                        if (evolved.fitness >= fitQuota || ++gen > evolved.trainCtx.maxGens)
                            resolve(evolved);

                        else {
                            if (!cur.fitness || evolved.fitness > cur.fitness) cur = evolved;
                            setTimeout(iter, 0);
                        }
                    });
                }
                
                setTimeout(iter, 0);
            });
        },

        trainAsync: function trainAsync(options = {}) {
            return new Promise((resolve) => {
                let generation = 1;
                let quota = options.fitnessQuota || 0.9;
                let cb = (options.inputSet == null || options.expectedSet == null) ? options.fitnessCallback : function (n) {
                    return n.staticFitness(options.inputSet, options.expectedSet);
                };
                let pool = new Array(options.population || 50).fill(0).map(() => w.mutate());

                options.maxGens = (options.maxGens != null ? options.maxGens : 500);

                function iterGen() {
                    return new Promise((reso) => {
                        let fits = {};
                        let scores = [];

                        pool.map((n, i, a) => function() {
                            return new Promise((re) => {
                                Promise.resolve(cb(n, generation)).then((score) => {
                                    fits[n.id] = score;
                                    
                                    if (options.debug)
                                        (options.log || process.stdout.write.bind(process.stdout).bind(process.stdout))(`\r(${i + 1}/${a.length})`);

                                    scores.push(score);
                                    re();
                                });
                            });
                        }).reduce((a, b) => a.then(b), Promise.resolve()).then(() => {
                            pool = pool.sort((a, b) => fits[b.id] - fits[a.id]);

                            let maxFit = Math.max.apply(Math, Object.keys(fits).map((k) => fits[k]));
                            let worstFit = Math.min.apply(Math, Object.keys(fits).map((k) => fits[k]));

                            if (options.debug)
                                (options.log || process.stdout.write.bind(process.stdout))(`\r[Generation #${generation}] Best fitness: ${maxFit} | Worst fitness: ${worstFit}\n`);

                            if (maxFit >= quota)
                                return resolve(pool[0]);

                            let desiredLength = pool.length;

                            pool = pool.slice(0, Math.ceil(pool.length * (options.survivalRate || 0.5)));
                            let best = pool.slice(0, Math.ceil(pool.length * (options.elitism != null ? options.elitism : 0.15))).concat(pool.slice(Math.ceil(pool.length * (options.elitism != null ? options.elitism : 0.15))).map((n) => n.mutate(options.mutation || null)));
                            pool = Array.from(best);

                            while (pool.length < desiredLength)
                                pool.push(greataptic.breed(best));

                            if (options.generationCallback != null)
                                options.generationCallback({
                                    gen: generation,
                                    best: pool[0],
                                    bestFit: maxFit,
                                    worstFit: worstFit,
                                    pool: pool
                                });

                            if (generation <= options.maxGens) {
                                generation++;
                                process.nextTick(function() {
                                    iterGen().then((best) => reso(best));
                                });
                            } else {
                                let best = pool[0];
                                return reso(best);
                            }
                        });
                    });
                }

                iterGen().then((best) => resolve(best));
            });
        },
    };

    return w;
};

greataptic.sequential = function sequential(inputSize, layers) {
    if ( layers.length === 0 ) {
        layers = [{ size: inputSize, post: 'sigmoid' }];
    }

    let res = {
        layers: {},
        first: '1'
    };

    let lastLayer = null;
    let lastSize = null;
    
    function buildLayer(l, index, after) {
        let subres = [];
        let postAfter = after;
        let preAfter = index + 'm';
        let midAfter = (l.post == null ? postAfter : index + 'p');
        let preId = index;
        let midId = (l.pre == null ? index : index + 'm');
        let postId = index + 'p';
        let subs = 0;

        function addLayer(l, id, size) {
            lastSize = size;
            lastLayer = l;
            l.name = id;
            subres.push(l);
            res.layers[id] = l;
        }

        let layerInput = lastSize == null ? inputSize : lastSize;
        
        if (l.pre != null) {
            addLayer(types[l.pre].build(index, preAfter), preId, layerInput);
            layerInput = lastSize == null ? inputSize : lastSize;
        }

        if (l.type === 'combo') {
            addLayer(types.combo.build(l.parts.map((ls, i, a) => types.sequence.build(buildLayer(ls, index + '_sub' + (++subs), (
                i + 1 === a.length ? null : index + '_sub' + (subs + 1)
            )), null), null), midAfter), midId, l.parts.reduce((a, ls) => a + ls.size, 0));
        }

        else if (l.type === 'sequence')
            addLayer(types.sequence.build(l.parts.map((ls, i, a) => buildLayer(ls, index + '_sub' + (++subs), (
                i + 1 === a.length ? null : index + '_sub' + (subs + 1)
            ), null)).reduce((a, b) => a.concat(b), []), midAfter), midId, l.parts.reduce((a, b) => b.size || a, null));

        else if (l.size != null) {
            if (l.type === 'spiking')
                addLayer(types.spiking.build(
                    new Array(l.size).fill(0).map(() => ({
                        weights: $vec.random(layerInput).data,
                        power: 0,
                        output: Math.pow(Math.random() * 3, 2),
                        limit: _logit(Math.random() * 0.45 + 0.5)
                    })),
                    midAfter
                ), midId, l.size);

            else if (l.type === 'linear' || l.type == null) {
                addLayer(types.linear.build(
                    new Array(l.size).fill(0).map(() => ({
                        offset: _logit(0.225 + 0.55 * Math.random()),
                        weights: $vec.random(layerInput).data
                    })),
                    midAfter
                ), midId, l.size);
            }

            else if (l.type === 'square')
                addLayer(types.square.build(
                    new Array(l.size).fill(0).map(() => ({
                        offset: _logit(0.25 + 0.5 * Math.random()),
                        weights: {
                            linear: $vec.random(layerInput).data,
                            square: $vec.random(layerInput).data
                        }
                    })),
                    midAfter
                ), midId, l.size);

            else if (l.type.startsWith('lstm') && types[l.type])
                addLayer(types[l.type].build(
                    types[l.type].randomGates(lastSize, l.size),
                    types[l.type].randomStates(l.size),
                    midAfter,
                    l.activation || 'sigmoid',
                    l.stateActivation || 'tanh'
                ), midId, l.size);
        }

        else
            addLayer(types[l.type].build(midAfter), midId, layerInput);

        if (l.post != null)
            addLayer(types[l.post].build(postAfter), postId, lastSize != null ? lastSize : layerInput);

        return subres;
    }

    layers.forEach((l, i) => buildLayer(l, '' + (i + 1), '' + (i + 2)));

    lastLayer.next = null;
    return greataptic.$net(res);
};

greataptic.fromJSON = function fromJSON(j) {
    return greataptic.$net(JSON.parse(j));
};

function inputNoise(size, options = {}) {
    let range = options.range || 1;
    let negative = (options.negative == null || options.negative);

    return new Array(size).fill(0).map(() => Math.random() * (negative ? range * 2 : range) - (negative ? range : 0));
}

greataptic.createVectorifier = function createVectorifier(arguments) {
    let vectorifier = {
        arguments: new Map(),

        getSize: function () {
            let res = 0;

            vectorifier.arguments.forEach((a) => {
                res += a.getSize();
            });

            return res;
        },

        encode: function (obj) {
            let enc = [];

            vectorifier.arguments.forEach((a) => {
                if (obj[a.name] === undefined)
                    throw new Error(`Required argument ${a.name} not passed to vectorizer!`);

                else
                    enc = enc.concat(a.encode(obj[a.name]));
            });

            return enc;
        },

        decode: function (vect) {
            let cursor = 0;
            let res = {};

            vectorifier.arguments.forEach((a) => {
                res[a.name] = a.decode(vect.slice(cursor, cursor + a.getSize()));
                cursor += a.getSize();
            });

            return res;
        }
    };

    arguments.forEach((arg) => {
        let aname = arg.name;
        const strspace = ' 0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

        if (arg.type.toLowerCase() === 'simplestring')
            vectorifier.arguments.set(aname, {
                name: aname,

                getSize: function getSize() {
                    return arg.size;
                },

                encode: function encode(s) {
                    let sl = s.slice(0, arg.size).split('').map((x) => strspace.indexOf(x) === -1 ? strspace[0] : x).join('');

                    while (sl.length < arg.size)
                        sl += arg.default || ' ';

                    return Array.from(sl).map((c) => {
                        return strspace.indexOf(c) / strspace.length;
                    });
                },

                decode: function decode(v) {
                    let x = v.map((i) => strspace[Math.floor(i * strspace.length)]).join('');
                    return x;
                }
            });

        else if (arg.type.toLowerCase() === 'number')
            vectorifier.arguments.set(aname, {
                name: aname,

                getSize: function getSize() {
                    return 1;
                },

                encode: function encode(n) {
                    return [(n - arg.min) / (arg.max - arg.min)];
                },
                
                decode: function decode(v) {
                    let res = v[0] * (arg.max - arg.min) + arg.min;
                    if (arg.rounded) res = Math.round(res);
                    
                    return res;
                }
            });

        else if (arg.type.toLowerCase() === 'numbers') {
            vectorifier.arguments.set(aname, {
                name: aname,

                getSize: function getSize() {
                    return arg.size;
                },

                encode: function encode(a) {
                    a = a.slice(0, arg.size);

                    while (a.length < arg.size)
                        a.push(0);

                    return a.map((n) => (n - arg.min) / (arg.max - arg.min));
                },

                decode: function decode(v) {
                    return v.map((n) => {
                        let res = n * (arg.max - arg.min) + arg.min;
                        if (arg.rounded) res = Math.round(res);
                        
                        return res;
                    });
                }
            });
        } else if (arg.type.toLowerCase() === 'string')
            vectorifier.arguments.set(aname, {
                name: aname,

                getSize: function getSize() {
                    return arg.size;
                },

                encode: function encode(s) {
                    let sl = s.slice(0, arg.size);

                    while (sl.length < arg.size)
                        sl += arg.default || ' ';

                    return Array.from(sl).map((c) => {
                        return c.charCodeAt(0) / 256;
                    });
                },

                decode: function decode(v) {
                    return v.map((i) => String.fromCharCode(Math.floor(i * 256))).join('');
                }
            });
    });

    return vectorifier;
};

greataptic.GAN = class GAN {
    constructor(properties) {
        properties.size = properties.size || {
            output: 40
        };

        this.size = {
            noise: +properties.size.noise || 15,
            output: +properties.size.output
        };

        this.generator = greataptic.sequential(this.size.noise, (properties.generatorLayers || this.generatorDefaultLayers()).concat([{
            size: this.size.output,
            type: properties.outputType || 'linear',
            post: 'sigmoid'
        }]));
        this.discriminator = greataptic.sequential(this.size.output, (properties.discriminatorLayers || this.discriminatorDefaultLayers()).concat([{
            size: 1,
            post: 'sigmoid'
        }]));
        this.noiseOptions = properties.noise || {};
    }

    generatorDefaultLayers() {
        return [
            {
                size: this.size.noise * 2 + 10,
                post: 'sigmoid'
            },
            {
                size: Math.ceil(this.size.noise * 1.5 + this.size.output) + 1,
                post: 'sigmoid'
            }
        ];
    }

    discriminatorDefaultLayers() {
        return [
            {
                size: Math.ceil(this.size.noise * 2 + this.size.output) + 5,
                post: 'sigmoid'
            },
            {
                size: Math.ceil(this.size.output / 1.5) + 2,
                post: 'sigmoid'
            },
        ];
    }

    rate(data) {
        let input = Array.from(data.data || data).concat(new Array(Math.max(0, this.size.output - data.length)).fill(0)).slice(0, this.size.output);
        return this.discriminator.compute(input).data[0];
    }

    evolve(realData, options) {
        let fake = [];
        let _step = options.step;
        let _postStep = options.postStep;

        Object.assign(options, {
            step: (net) => {
                let f = this.generate(net);
                fake.push(f);
                
                if (_step != null) _step(net);

                return this.rate(f);
            },

            postStep: (net) => {
                this._evolveCycleDiscGrade(realData, fake, (options.discriminatorTrainOptions || {}).comparisonSize || 4, options.discriminatorTrainOptions || null);
                net.fake = fake;
                if (_postStep != null) _postStep(net);
            }
        });

        return this.generator.evolve(options).then((newGenerator) => {
            this.generator = newGenerator;
        });
    }

    _evolveCycleDiscGrade(realData, fakeData, maxComparisonSize = 4, discriminatorOptions = null) {
        maxComparisonSize = Math.min(maxComparisonSize, realData.length, fakeData.length);

        realData = shuffle(realData, {
            copy: true
        }).slice(0, maxComparisonSize);
        fakeData = shuffle(fakeData, {
            copy: true
        }).slice(0, maxComparisonSize);

        let expec = new Array(realData.length).fill([1]);
        let notExpec = new Array(realData.length).fill([0]);

        let _opts = {
            inputSet: realData.concat(fakeData),
            expectedSet: expec.concat(notExpec),
            maxGens: 15,
            maxMutation: 0.3,
            population: 30
        };

        Object.assign(_opts, discriminatorOptions);

        return this.discriminator.evolve(_opts).then((disc) => {
            this.discriminator = disc;
        });
    }

    train(realData, options) {
        let fake = [];

        Object.assign(options, {
            generationCallback: (data) => {
                data.fake = fake;
                if (options.genCallback != null) options.genCallback(data);
                this._trainCycleDiscGrade(realData, fake, options.maxComparisonSize || undefined, options.discriminatorTrainOptions || null);

                fake = [];
            },

            fitnessCallback: (n, i) => {
                if (i === 1) {
                    fake.push(this.generate(n));
                    return 0;
                }

                let res = this._trainCycleFitness(n);

                if (fake.length < (options.discriminatorTrainOptions || {}).comparisonSize || 4)
                    fake.push(res.gen);

                return res.fit;
            },
        });

        this.generator = this.generator.train(options);
    }

    trainAsync(realData, options) {
        return new Promise((resolve) => {
            let fake = [];

            Object.assign(options, {
                generationCallback: (data) => {
                    data.fake = fake;
                    if (options.genCallback != null) options.genCallback(data);
                    this._trainCycleDiscGradeAsync(realData, fake, options.maxComparisonSize || undefined, options.discriminatorTrainOptions || null);

                    fake = [];
                },

                fitnessCallback: (n, i) => {
                    if (i === 1) {
                        fake.push(this.generate(n));
                        return 0;
                    }

                    let res = this._trainCycleFitness(n);

                    if (fake.length < (options.discriminatorTrainOptions || {}).comparisonSize || 4)
                        fake.push(res.gen);

                    return res.fit;
                },
            });

            this.generator.trainAsync(options).then((best) => {
                this.generator = best;
                resolve(best);
            });
        });
    }

    _trainCycleDiscGrade(realData, fakeData, maxComparisonSize = 4, discriminatorOptions = null) {
        maxComparisonSize = Math.min(maxComparisonSize, realData.length, fakeData.length);

        realData = shuffle(realData, {
            copy: true
        }).slice(0, maxComparisonSize);
        fakeData = shuffle(fakeData, {
            copy: true
        }).slice(0, maxComparisonSize);

        let expec = new Array(realData.length).fill([1]);
        let notExpec = new Array(realData.length).fill([0]);

        let _opts = {
            inputSet: realData.concat(fakeData),
            expectedSet: expec.concat(notExpec),
            maxGens: 10,
            mutation: 0.4,
            elitism: 0.02,
            survivalRate: 0.2,
            population: 15
        };

        Object.assign(_opts, discriminatorOptions);
        this.discriminator = this.discriminator.train(_opts);
    }

    _trainCycleDiscGradeAsync(realData, fakeData, maxComparisonSize = 4, discriminatorOptions = null) {
        maxComparisonSize = Math.min(maxComparisonSize, realData.length, fakeData.length);

        realData = shuffle(realData, {
            copy: true
        }).slice(0, maxComparisonSize);
        fakeData = shuffle(fakeData, {
            copy: true
        }).slice(0, maxComparisonSize);

        let expec = new Array(realData.length).fill([1]);
        let notExpec = new Array(realData.length).fill([0]);

        let _opts = {
            inputSet: realData.concat(fakeData),
            expectedSet: expec.concat(notExpec),
            maxGens: 10,
            mutation: 0.4,
            elitism: 0.02,
            survivalRate: 0.2,
            population: 15
        };

        Object.assign(_opts, discriminatorOptions);
        this.discriminator.trainAsync(_opts).then((disc) => {
            this.discriminator = disc;
        });
    }

    _trainCycleFitness(generatorNetwork) {
        let fakeData = this.generate(generatorNetwork);
        return {
            gen: fakeData,
            fit: this.rate(fakeData)
        };
    }

    makeNoise() {
        return inputNoise(this.size.noise, this.noiseOptions);
    }

    generate(net = null) {
        let noise = this.makeNoise();
        let res = (net != null ? net : this.generator).compute(noise).data;

        res.noise = noise;
        return res;
    }
};

greataptic.StaticMultiEvolver = class StaticMultiEvolver {
    constructor(nets, processor) {
        if (nets instanceof Map)
            this.nets = nets;

        else
            this.nets = new Map(Object.entries(nets));

        this.processor = processor;
    }

    cloneNets() {
        let res = new Map();

        this.nets.forEach((net, name) => {
            res.set(name, net.clone());
        });

        return res;
    }

    compute(input, netMap = this.nets) {
        let nets = {};

        netMap.forEach((net, name) => {
            nets[name] = net;
        });

        return Promise.resolve(this.processor(nets, input));
    }

    error(nets, inputSet, expectedSet) {
        return new Promise((resolve) => {
            let outputProms = inputSet.map((i) => this.compute(i, nets));

            Promise.all(outputProms).then((outputSet) => {
                let res = outputSet.map((os, si) => os.data.map((o, i) => Math.pow((o - expectedSet[si][i]), 2)).reduce((a, b) => a + b, 0)).reduce((a, b) => a + b, 0);

                resolve(res / inputSet.length);
            });
        });
    }

    staticFitness(nets, inputSet, expectedSet) {
        return this.error(nets, inputSet, expectedSet).then((errVal) => {
            return 1 / (1 + errVal);
        });
    }

    getFitness(nets, inputSet, expectedSet) {
        return this.staticFitness(nets, inputSet, expectedSet);
    }

    evolveStatic(inputSet, expectedSet, options = {}) {
        let fitQuota = options.quota != null ? options.quota : Infinity;
        let cur = this.cloneNets();
        let gen = 1;

        options.random = options.random || false;
        
        return new Promise((resolve) => {
            let iter = () => {
                let proms = [];
                let evolved = new Map();

                this.getFitness(cur, inputSet, expectedSet).then((oldFit) => {
                    cur.forEach((net, name) => {
                        let thisIterOptions = {};
                        Object.assign(thisIterOptions, [options, {
                            step: (stepNet) => {
                                let oldNet = cur[name];
                                cur[name] = stepNet;
                                this.getFitness(cur, inputSet, expectedSet).then((fit) => {
                                    cur[name] = oldNet;
                                    return fit;
                                });
                            }
                        }]);

                        proms.push(net.evolveStep(thisIterOptions).then((net) => {
                            evolved.set(name, net);
                        }));
                    });

                    Promise.all(proms).then(() => {
                        this.getFitness(evolved, inputSet, expectedSet).then((newFit) => {
                            options.random = false;

                            if (options.debug) // print debug status
                                console.log(`[DEBUG] Generation ${gen}/${evolved.trainCtx.maxGens}: fitness is now ${newFit}.`);

                            if (newFit >= fitQuota || ++gen > (options.maxGens || 500)) {
                                this.nets = evolved;
                                resolve(evolved);
                            }

                            else {
                                if (newFit > oldFit) cur = evolved;
                                process.nextTick = iter;
                            }
                        });
                    });
                });
            };
            
            process.nextTick = iter;
        });
    }
};

module.exports = greataptic;
}).call(this,require('_process'))
},{"./lib/vector.js":"ga-vector","_process":2,"shuffle-array":3,"util":5}]},{},[]);
